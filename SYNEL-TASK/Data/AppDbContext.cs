﻿using Microsoft.EntityFrameworkCore;
using SYNEL_TASK.Models;

namespace SYNEL_TASK.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {  }

        public DbSet<Employee> Employees { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasKey(m => m.Id);

            modelBuilder.Entity<Employee>()
                .Property(m => m.Date_of_Birth)
                .HasColumnType("date");

            modelBuilder.Entity<Employee>()
                .Property(m => m.Start_Date)
                .HasColumnType("date");
        }
    }
}
