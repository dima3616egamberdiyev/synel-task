﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SYNEL_TASK.Interface;
using SYNEL_TASK.Models;
using SYNEL_TASK.Utils;

namespace SYNEL_TASK.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository; 

        public EmployeesController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        // GET: Employees
        public IActionResult Index()
        {
            return View();
        }
        // POST: Employees/Index
        [HttpPost]
        public async Task<JsonResult> Index(DataTableParameters dataTableParameters)
        {
            DataTableResult<List<Employee>> res = null;
            try
            {
                var searchValue = dataTableParameters.Search.Value;
                int recordsTotal = 0;
                var employeeData = (from employee in _employeeRepository.GetAll() select employee);

                switch (dataTableParameters.Order.FirstOrDefault()?.Column)
                {
                    case 0:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Surname) : employeeData.OrderByDescending(s => s.Surname);
                        break;
                    case 1:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Forenames) : employeeData.OrderByDescending(s => s.Forenames);
                        break;
                    case 2:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Payroll_Number) : employeeData.OrderByDescending(s => s.Payroll_Number);
                        break;
                    case 3:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Date_of_Birth) : employeeData.OrderByDescending(s => s.Date_of_Birth);
                        break;
                    case 4:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Telephone) : employeeData.OrderByDescending(s => s.Telephone);
                        break;
                    case 5:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Mobile) : employeeData.OrderByDescending(s => s.Mobile);
                        break;
                    case 6:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Address) : employeeData.OrderByDescending(s => s.Address);
                        break;
                    case 7:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Address_2) : employeeData.OrderByDescending(s => s.Address_2);
                        break;
                    case 8:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Postcode) : employeeData.OrderByDescending(s => s.Postcode);
                        break;
                    case 9:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.EMail_Home) : employeeData.OrderByDescending(s => s.EMail_Home);
                        break;
                    case 10:
                        employeeData = dataTableParameters.Order.FirstOrDefault()?.Dir == "asc" ? employeeData.OrderBy(s => s.Start_Date) : employeeData.OrderByDescending(s => s.Start_Date);
                        break;
                    default:
                        employeeData = employeeData.OrderBy(s => s.Surname);
                        break;
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    employeeData = employeeData.Where(m => m.Payroll_Number.Contains(searchValue)
                                                || m.Forenames.Contains(searchValue)
                                                || m.Surname.Contains(searchValue)
                                               // || (m.Date_of_Birth.ToString()).Contains(searchValue)
                                                || m.Telephone.Contains(searchValue)
                                                || m.Mobile.Contains(searchValue)
                                                || m.Address.Contains(searchValue)
                                                || m.Address_2.Contains(searchValue)
                                                || m.Postcode.Contains(searchValue)
                                                || m.EMail_Home.Contains(searchValue)
                                              // || m.Start_Date.ToString().Contains(searchValue)
                                               );
                }
                recordsTotal = employeeData.Count();
                var data = await employeeData.Skip(dataTableParameters.Start).Take(dataTableParameters.Length).ToListAsync();
                res = new DataTableResult<List<Employee>>{ Draw = dataTableParameters.Draw, RecordsFiltered = recordsTotal, RecordsTotal = recordsTotal, Data = data };
            }
            catch (Exception ex)
            {
                res = new DataTableResult<List<Employee>> { Error = ex.Message };
            }
            return Json(res);
        }
        // GET: Employees/AddFile
        [HttpGet]
        public IActionResult AddFile()
        {
            return View();
        }
        // POST: Employees/AddFile
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddFile([Bind("File")] EmployeeFile model)
        {
            if (ModelState.IsValid)
            {
                List<Employee> employees = Util.ParseCSVToEmployee(model.File);
                await _employeeRepository.AddRange(employees);
                return View("AddedRows", employees);
            }
            return View(model);
        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _employeeRepository.GetById(id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Payroll_Number,Forenames,Surname,Date_of_Birth,Telephone,Mobile,Address,Address_2,Postcode,EMail_Home,Start_Date")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                await _employeeRepository.Add(employee);
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _employeeRepository.GetById(id);
            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Payroll_Number,Forenames,Surname,Date_of_Birth,Telephone,Mobile,Address,Address_2,Postcode,EMail_Home,Start_Date")] Employee employee)
        {
            if (id != employee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _employeeRepository.Update(employee);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_employeeRepository.EmployeeExists(employee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _employeeRepository.GetById(id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            await _employeeRepository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
