﻿using Microsoft.EntityFrameworkCore;
using SYNEL_TASK.Data;
using SYNEL_TASK.Interface;
using SYNEL_TASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SYNEL_TASK.Repository
{
    /// <summary>
    /// class that works with the database
    /// </summary>
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext _context;

        public EmployeeRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task Add(Employee model)
        {
            _context.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task AddRange(List<Employee> listModel)
        {
            _context.AddRange(listModel);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(long? id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
        }

        public bool EmployeeExists(long id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }

        public IQueryable<Employee> GetAll()
        {
            return _context.Employees;
        }

        public async Task<Employee> GetById(long? id)
        {
            return await _context.Employees
                   .FirstOrDefaultAsync(m => m.Id == id);
        }

        public void Search()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Employee model)
        {
            _context.Update(model);
            await _context.SaveChangesAsync();
        }
    }
}
