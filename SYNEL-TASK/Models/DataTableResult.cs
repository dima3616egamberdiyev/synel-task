﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SYNEL_TASK.Models
{
    /// <summary>
    /// Response data to Jquery DataTable
    /// </summary>
    public class DataTableResult<T>
    {
        public int Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public T Data { get; set; }
        public string Error { get; set; }
    }
}
