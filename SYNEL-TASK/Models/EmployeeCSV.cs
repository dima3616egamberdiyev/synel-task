﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace SYNEL_TASK.Models
{
    public class EmployeeCSV 
    {
        public string Payroll_Number { get; set; }
        public string Forenames { get; set; }
        public string Surname { get; set; }

       // [DataType(DataType.Date)]
      //  public DateTime? Date_of_Birth { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Address_2 { get; set; }
        public string Postcode { get; set; }

        //[DataType(DataType.EmailAddress)]
       // public string EMail_Home { get; set; }

        //[DataType(DataType.Date)]
        //public DateTime? Start_Date { get; set; }
    }
}
