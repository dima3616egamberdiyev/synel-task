﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SYNEL_TASK.Models
{
    public class EmployeeFile
    {
        [Display(Name = "Choose File")]
        [Required]
        [FileControllValidation("csv", ErrorMessage = "File format not correct. csv is required")]
        public IFormFile File { get; set; }
    }
}
