﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SYNEL_TASK.Models
{
    /// <summary>
    /// Class created to map CSV file to Employee
    /// </summary>
    public sealed class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            //  Map(m => m.Id);
            Map(m => m.Payroll_Number).Index(0);
            Map(m => m.Forenames).Index(1);
            Map(m => m.Surname).Index(2);
            Map(m => m.Date_of_Birth).Index(3);
            Map(m => m.Telephone).Index(4);
            Map(m => m.Mobile).Index(5);
            Map(m => m.Address).Index(6);
            Map(m => m.Address_2).Index(7);
            Map(m => m.Postcode).Index(8);
            Map(m => m.EMail_Home).Index(9);
            Map(m => m.Start_Date).Index(10);
        }
    }
}
