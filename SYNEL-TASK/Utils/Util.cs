﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Microsoft.AspNetCore.Http;
using SYNEL_TASK.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace SYNEL_TASK.Utils
{
    public static class Util
    {
        /// <summary>
        /// method that maps the received CSV file
        /// </summary>
        /// <param name="formFile"></param>
        /// <returns></returns>
        public static List<Employee> ParseCSVToEmployee(IFormFile formFile)
        {
            List<Employee> empList;
            using var fileReader = new StreamReader(formFile.OpenReadStream());
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HeaderValidated = null,
                MissingFieldFound = null,
                MemberTypes = MemberTypes.Fields
            };
            using var csv = new CsvReader(fileReader, config);
            var options = new TypeConverterOptions { Formats = new[] { "dd/M/yyyy" } };
            csv.Context.TypeConverterOptionsCache.AddOptions<DateTime?>(options);
            csv.Context.TypeConverterOptionsCache.AddOptions<DateTime>(options);
            csv.Context.RegisterClassMap<EmployeeMap>();
            csv.Read();
            csv.ReadHeader();
            empList = csv.GetRecords<Employee>().ToList();
            return empList;
        }
    }
}
