﻿using SYNEL_TASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SYNEL_TASK.Interface
{
    public interface IEmployeeRepository
    {
        Task Add(Employee model);
        Task AddRange(List<Employee> model);
        Task Update(Employee model);
        Task Delete(long? id);
        Task<Employee> GetById(long? id);
        IQueryable<Employee> GetAll();
        void Search();
        bool EmployeeExists(long id);
    }
}
